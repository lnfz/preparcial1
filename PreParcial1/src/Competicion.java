import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public abstract class Competicion 
{

	private String nombre;
	private ArrayList<Participantes> participantes; 
	/*se hace como atributo para que sus hijos tengan como parametro de entrada
	 *el arreglo de participantes, el cual se crea en el main y se introduce
	 *en los parametros de GRUPO, LIGA Y ELIMINACION DIRECTA 
	 */
	
	public Competicion(String nombre, ArrayList<Participantes> participantes) {
		this.setNombre(nombre);
		this.setParticipantes(participantes);
		Collections.shuffle(participantes); //hace que el arreglo se desordene aleatoriamente
	}

	public abstract void armarFixture();
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Participantes> getParticipantes() {
		return participantes;
	}

	public void setParticipantes(ArrayList<Participantes> participantes) {
		this.participantes = participantes;
	}
	
}
