import java.util.ArrayList;
import java.util.Random;

public class EliminacionDirecta extends Competicion {

	public EliminacionDirecta(String nombre, ArrayList<Participantes> participantes) {
		super(nombre, participantes);
	}

	@Override
	public void armarFixture() 
	{
		ArrayList<Participantes> gr1 = new ArrayList<Participantes>(this.getParticipantes().subList(0, 8));
		ArrayList<Participantes> gr2 = new ArrayList<Participantes>(this.getParticipantes().subList(8, 16));
		ArrayList<Participantes> grg = new ArrayList<Participantes>();
		Integer r = new Random().nextInt(2);
		
		//muestra los primero 8 partidos
		System.out.println("Octavos de final");
		for (int i = 0; i < gr2.size(); i++) {
			System.out.println(gr1.get(i).getNombre() + gr1.get(i).getNumeroParticipante() + " vs " + gr2.get(i).getNombre() + gr2.get(i).getNumeroParticipante());
		}
		
		//pasa a ambos array a un array general que contiene solo a los ganadores
		for (int i = 0; i < 8; i++) {
			if (r == 1) {
				if (gr1.isEmpty()==false) {
				grg.add(gr1.get(i));
				}
				else 
				{
					grg.add(gr2.get(i));	
				}
				}
				else
				{
					if (gr2.isEmpty()==false) {
						grg.add(gr2.get(i));
					}
					else
					{
						grg.add(gr1.get(i));
					}
				}	
		}
		
		//espacio vacio entre textos
		System.out.println(" ");
		
		//limpia los dos array
		gr1.clear();
		gr2.clear();
		//pasa la mitad del array general a los dos array
		for (int i = 0; i < 4; i++) {
			gr1.add(grg.get(i));	
		}
		for (int i = 4; i < 8; i++) {
			gr2.add(grg.get(i));
		}
		
		//muestra los partidos del cuarto de final
		System.out.println("Cuartos de final");
		for (int i = 0; i < gr2.size(); i++) {
			System.out.println(gr1.get(i).getNombre() + gr1.get(i).getNumeroParticipante() + " vs " + gr2.get(i).getNombre() + gr2.get(i).getNumeroParticipante());
			}
		
		for (int i = 0; i < 4; i++) {
			if (r == 1) {
				if (gr1.isEmpty()==false) {
				grg.add(gr1.get(i));
				}
				else 
				{
					grg.add(gr2.get(i));	
				}
				}
				else
				{
					if (gr2.isEmpty()==false) {
						grg.add(gr2.get(i));
					}
					else
					{
						grg.add(gr1.get(i));
					}
				}	
		} 
		
		//espacio vacio entre textos
		System.out.println(" ");
								
		//limpia los dos array				
		gr1.clear();				
		gr2.clear();
		//pasa la mitad del array general a los dos array				
		for (int i = 0; i < 2; i++) {					
			gr1.add(grg.get(i));	
			}
		for (int i = 2; i < 4; i++) {
			gr2.add(grg.get(i));
			}
				
		//muestra la semi final
		System.out.println("Semi final");
		for (int i = 0; i < gr2.size(); i++) {
			System.out.println(gr1.get(i).getNombre() + gr1.get(i).getNumeroParticipante() + " vs " + gr2.get(i).getNombre() + gr2.get(i).getNumeroParticipante());
			}
		
		for (int i = 0; i < 2; i++) {
			if (r == 1) {
				if (gr1.isEmpty()==false) {
				grg.add(gr1.get(i));
				}
				else 
				{
					grg.add(gr2.get(i));	
				}
				}
				else
				{
					if (gr2.isEmpty()==false) {
						grg.add(gr2.get(i));
					}
					else
					{
						grg.add(gr1.get(i));
					}
				}	
		}
		
		//espacio vacio entre textos
		System.out.println(" ");
				
		//limpia los dos array
		gr1.clear();
		gr2.clear();
		//pasa la mitad del array general a los dos array
		for (int i = 0; i < 1; i++) {
			gr1.add(grg.get(i));	
			}
		for (int i = 1; i < 2; i++) {
			gr2.add(grg.get(i));
			}
				
		//muestra la final
		System.out.println("Final");
		for (int i = 0; i < gr2.size(); i++) {
			System.out.println(gr1.get(i).getNombre() + gr1.get(i).getNumeroParticipante() + " vs " + gr2.get(i).getNombre() + gr2.get(i).getNumeroParticipante());
			}
		
		for (int i = 0; i < 1; i++) {
			if (r == 1) {
				if (gr1.isEmpty()==false) {
				grg.add(gr1.get(i));
				}
				else 
				{
					grg.add(gr2.get(i));	
				}
				}
				else
				{
					if (gr2.isEmpty()==false) {
						grg.add(gr2.get(i));
					}
					else
					{
						grg.add(gr1.get(i));
					}
				}	
		}
		
		//espacio entre textos
		System.out.println(" ");
		
		for (int j = 0; j < 1; j++) {
			System.out.println("Ganador: " + grg.get(j).getNombre() + grg.get(j).getNumeroParticipante());	
		}
		}
	}
