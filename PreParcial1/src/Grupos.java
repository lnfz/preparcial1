import java.util.ArrayList;

public class Grupos extends Competicion {

	public Grupos(String nombre, ArrayList<Participantes> participantes) {
		super(nombre, participantes);
	}

	@Override
	public void armarFixture() 
	{
		Integer j = 1;//por estetica, para que se ponga el numero del grupo
		for (int i = 0; i < getParticipantes().size(); i++) //getParticipantes().size() obtiene la longitud
		{
			if (i<1 || i==4 || i == 8 || i == 12) //esto sirve para separar de a 4, empezando por el 0-3,4-7,8-11,12-15
			{
				System.out.println("Grupo " + j + ":");
				j ++; //se suma para que en la siguiente vez que entre ya sea grupo "x"
			}
			System.out.println(getParticipantes().get(i).getNombre() + getParticipantes().get(i).getNumeroParticipante());
			//imprime nombre y numero del participante
		}
		System.out.println(" ");
	}

}
