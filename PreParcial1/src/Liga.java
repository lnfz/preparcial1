import java.util.ArrayList;

public class Liga extends Competicion {
	
	public Liga(String nombre, ArrayList<Participantes> participantes) {
		super(nombre, participantes);
	}

	@Override
	public void armarFixture() 
	{
		System.out.println("Fecha 1");
		for (int i = 0; i < getParticipantes().size(); i++) 
		{
			System.out.println(getParticipantes().get(i).getNombre() + getParticipantes().get(i).getNumeroParticipante() + 
					" vs " + getParticipantes().get(i+1).getNombre() + getParticipantes().get(i+1).getNumeroParticipante());
			i = i + 1;
		}
		System.out.println(" ");
	}

}
