import java.util.ArrayList;

public class main {

	public static void main(String[] args) 
	{
		
		ArrayList<Participantes> part = new ArrayList<Participantes>();
		
		for (int i = 0; i < 16; i++) //agrego participantes al arreglo
		{
			Participantes p = new Participantes (i+1, "Participante ");
			part.add(p);
		}
		
		Grupos g = new Grupos("Grupo",part); //agrega a una categoria (GRUPO) el arreglo de participantes
		
		g.armarFixture(); //imprime
		
		Liga l = new Liga ("Liga", part);
		
		l.armarFixture();
		
		EliminacionDirecta e = new EliminacionDirecta("Liga", part);
		
		e.armarFixture();
	}

}
